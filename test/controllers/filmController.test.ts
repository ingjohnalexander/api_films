const request = require('supertest');
const express = require('express');

import {getFilms,getFilmProm,setFilm,acceptedFilm, deleteFilm} from "../../films/controllers/filmController"
import { delete_film, accept_film, set_film, get_films_prom, get_films} from '../../service/film.service';


const app = express();

jest.mock("../../database/database", () => {
	return {

		myDataSource: {
            initialize: jest.fn().mockReturnValue({
                then:jest.fn().mockReturnValue({
                    catch: jest.fn().mockReturnValue({}),
                }),
            }),
			getRepository: jest.fn().mockReturnValue({                
				find: jest.fn().mockReturnValue(["test"]),
                createQueryBuilder: jest.fn().mockReturnValue(["test"]),
				findOneBy: jest.fn().mockReturnValue({  }),
				save: jest.fn().mockReturnValue([]),
				count: jest.fn().mockReturnValue(1),
				delete: jest.fn().mockReturnValue([]),
			}),
			manager: {
              
				getRepository: jest.fn().mockReturnValue({

					find: jest.fn().mockReturnValue(["test"]),
					findOneBy: jest.fn().mockReturnValue({ posts: [] }),
					save: jest.fn().mockReturnValue([]),
					count: jest.fn().mockReturnValue(1),
					delete: jest.fn().mockReturnValue([]),
                    createQueryBuilder: jest.fn().mockReturnValue({
                        from: jest.fn().mockReturnValue({
                            select: jest.fn().mockReturnValue({
                                where: jest.fn().mockReturnValue({
                                    getCount:jest.fn().mockReturnValue([]),
                                }),
                            }),
                            where: jest.fn().mockReturnValue({
                                select: jest.fn().mockReturnValue({
                                    getRawMany:jest.fn().mockReturnValue([]),
                                    orderBy:jest.fn().mockReturnValue({
                                        getCount:jest.fn().mockReturnValue([]),
                                        getRawMany: jest.fn().mockReturnValue([]),
                                        limit: jest.fn().mockReturnValue({
                                            offset: jest.fn().mockReturnValue({
                                                getRawMany: jest.fn().mockReturnValue([])
                                            })
                                        }),
                                    })
                                })
                            }),
                        }),
                    })                    
				})
			},

            repository:{
				createQueryBuilder: jest.fn().mockReturnValue({
					select: jest.fn().mockReturnValue({
						where: jest.fn().mockReturnValue({
							distinct: jest.fn().mockReturnValue({
								getRawMany: jest.fn().mockReturnValue([])
							})
						})
					})
				})  
            }

              


		}
	};
});


let data = {
    message: 'Se ha eliminado el registro',
    code: 200,
    value: [],
    canNotify: false,
    error: false
  };

describe('valid Controller Film',  () => {

    test('Listar Peliculas con paginacion y opcion de filtrar (Sin Autenticaciòn)', async() => {
        const req = {  };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
        }
        getFilms(req, res);
        data.message = "Lista Peliculas";
        const value = await get_films({page:1,search:"asdasd"});
        expect(res.status(200).send(value)).toMatchObject(data);
    });

    test('Obtener una pelicula y su promedio de calificacion (Sin Autenticaciòn)', async() => {
        const req = {  };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
        }
        getFilmProm(req, res);
        data.message = "Se ha obtenido la pelicula y su promedio";
        const value = await get_films_prom({id:1});
        expect(res.status(200).send(value)).toMatchObject(data);
    });

    test('Solicitar Agregar una Pelicula (Con autenticacion - user normal)', async() => {

        const req = { 
            headers:{
                authorization:""
            }            
         };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
            
        }
        const jwt = {
            getToken:jest.fn().mockReturnValue(""),
        }
        setFilm(req, res);
        data.message = "Registro Exitoso";
        const value = await set_film({description:"descscsc",image:"---",name:"name Pelicula"},{user:1});
        expect(res.status(200).send(value)).toMatchObject(data);
    });

    test('Aceptar pelicula a agregar (Con autenticacion -  admin)', async() => {

        const req = { 
            headers:{
                authorization:""
            }            
         };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
            
        }
        const jwt = {
            getToken:jest.fn().mockReturnValue(""),
        }
        acceptedFilm(req, res);
        data.message = "Actualizaciòn Exitosa";
        const value = await accept_film({id:1},{user:1});
        expect(res.status(200).send(value)).toMatchObject(data);
    }); 
    
    test('Eliminar pelicula (Con autenticacion -  admin)', async() => {

        const req = { 
            headers:{
                authorization:""
            }            
         };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
            
        }
        const jwt = {
            getToken:jest.fn().mockReturnValue(""),
        }
        deleteFilm(req, res);
        data.message = "Se ha eliminado la pelicula y sus comentarios";
        const value = await delete_film({id:1},{user:1})
        expect(res.status(200).send(value)).toMatchObject(data);
    });     


});
