import { Router } from "express";
import { authRoutes } from "./../auth/routes";
import { routesFilms } from "../films/routes/routesFilms";
import { routesComments } from "../films/routes/routesComments";


export const routes = (router:Router) => {
	authRoutes(router)
	routesFilms(router)
	routesComments(router)
};
