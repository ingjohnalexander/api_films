import { CustomResponse } from "../utils/interfaces";
import { Responses } from "../utils/responses";

import {User} from "../database/entities/User"
import {myDataSource} from "../database/database";
import * as service from "../utils/service";


/**
 *  register new user
 */

export const register = async (data:any): Promise<CustomResponse> => {

  try {
        const repositoryUser = myDataSource.manager.getRepository(User);
        let elements = await repositoryUser.findOneBy({email:data.email});
        let message = "";
        let success = "ERROR";
        if (elements == null) {

          if(data.username=='' || data.password == '' || data.username.length <= 3 || data.password.length <= 5 ){
            message = data.username=='' || data.username.length <=3  ? message +" <br> El usuario debe tener un minimo de 3 caracteres " : message +" ";
            message = data.password=='' || data.password.length <=5  ? message +" <br> El password debe tener un minimo de 6 caracteres": message +" ";
            message = data.email==''  ? message +" <br> No es un email correcto": message +" ";
            success="ERROR";
            return Responses.success({ value: {success:success},message:message });
          }
          success="OK";
          let userData = {
            username : data.username,
            email : data.email,
            password :  await service.setPass(data.password),
            roles_id : 2,
            status:1,
            create_at :new Date()
          };
          repositoryUser.save(userData);
        }else{
          success="ERROR";
          message = "Usuario ya se encuentrta registrado";
          return Responses.success({ value: {success:success},message:message });
        }

		return Responses.success({ value: {},message:"Registrado" });
	} catch (error:any) {
		return Responses.internalServerError({ message: error?.message });
	}
};
