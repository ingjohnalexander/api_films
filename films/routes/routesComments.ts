'use strict'
import { Router } from "express";
import * as commentController from "../controllers/commentController" 
import * as jwt from "../../utils/jwt";

export const baseRoute = "/"+process.env.ROUTE+"comment";
export const routesComments = (router: Router) => {


	/**
	 * GET /comment/{page}/{id}
	 * @tags Comments
	 * @summary get all commenets for id = id film and page
	 * @description get all comments
	 * @param {number} id.path - id
	 * @param {number} page.path - page
	* @return {object} 200 - Success response
	 * }
	 * @example response - 200 - example success response
	 * {
	 *   "message": ""
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.get(baseRoute+"/:page/:id",commentController.getComments);

	/**
	 * POST /comment/comment-set
	 * @tags Comments
	 * @summary register comment
	 * @description register comment
	 * @param {object} request.body.required - info - application/json
	 * @security BasicAuth | BearerAuth
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @example request - example user film
	 * {
     *   "description":"Comentario de pelicula",      
     *   "qualification":2,
     *   "film_id":5
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.post(baseRoute+"/comment-set",jwt.authenticate([1,2]),commentController.setComment);


	/**
	 * POST /comment/comment-get
	 * @tags Comments
	 * @summary register comment
	 * @description register comment
	 * @param {object} request.body.required - info - application/json
	 * @security BasicAuth | BearerAuth
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @example request - example user film
	 * {
    * "id":1,
    * "film_id":1
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.post(baseRoute+"/comment-get",jwt.authenticate([1,2]),commentController.getComment);	

	/**
	 * POST /comment/comment-delete
	 * @tags Comments
	 * @summary delete comment for id
	 * @description delete comment
	 * @param {object} request.body.required - info - application/json
	 * @security BasicAuth | BearerAuth
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @example request - example user film
	 * {
	 *   "id":1
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.post(baseRoute +"/comment-delete",jwt.authenticate([1]),commentController.deleteComment);

};
