'use strict'
import * as jwt from "jwt-simple"; 
import moment from 'moment';
const secret = process.env.JWT_KEY||"";



export const  createToken = async(user:number,role:number,nameRol:string) => {
    var payload = {
      sub: user,
      role:role,
      nameRol:nameRol
      //iat:moment().unix(),
     // exp:moment().add(30,'days').unix
    };
    return jwt.encode(payload,process.env.JWT_KEY||"");
}



export const authenticate = (access:number[]) => {
	return (req:any, res:any, next:any) => {
        if(!req.headers.authorization){
            return res.status(401).send({message:"No tiene un Token Valido"});
        }
        try{
        //var token = req.headers.authorization.replace(/['"]+/g,'');
        var token = req.headers.authorization.replace("Bearer ",'');
        var payload = jwt.decode(token,process.env.JWT_KEY||"");
    
            if(payload.exp<=moment().unix()){
                return res.status(401).send({message:"Token ha expirado"});
            }
            if(!access.includes(payload.role)){
                return res.status(401).send({message:"No tiene acceso a este servicio"});
            }
        }catch(ex){
            return res.status(401).send({message:"Error en el Token"});
        }
        req.user = payload;
        next();
	};
};



export const getToken = async(token:string) => {
    try {
        var decoded = jwt.decode(token, process.env.JWT_KEY||"");        
        return {
            sub: decoded.sub,
            role:decoded.role,
            nameRol:decoded.nameRol            
        };
      } catch (err) {
        return null;
     }    
}
