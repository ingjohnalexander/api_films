import { CustomResponse } from "../utils/interfaces";
import { Responses } from "../utils/responses";

import {User} from "./../database/entities/User"
import {myDataSource} from "../database/database";
import * as service from "./../utils/service";
import * as jwt from "./../utils/jwt";


/**
 * Login User
*/

export const auth = async (email:string,password:any): Promise<CustomResponse> => {

  try {
        const repository = myDataSource.manager.getRepository(User);
        let elements = await repository.findOneBy({email:email});

        let token = '';
        let role = 0;
        let message = "";
        let success = "ERROR"
        if (elements==null)
          return Responses.success({ message: "No se encontraron datos", value: [] });
        
        let valid = await service.getPass(password, elements.password);

        success = valid ? 'OK':'ERROR';
        message = valid ? '':'ERROR';
        token = valid ? await jwt.createToken(elements.id,elements.roles_id,"---") : "";
        role = valid ? elements.roles_id : 0;
        

		return Responses.success({ value: {username:elements.username,success:success,token:token,role:role},message:"Logueado" });
	} catch (error:any) {
		return Responses.internalServerError({ message: error?.message });
	}
};
