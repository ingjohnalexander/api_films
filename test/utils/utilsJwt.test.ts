import * as dotenv from 'dotenv'
dotenv.config()
import {createToken, getToken, authenticate} from "../../utils/jwt"



describe('jwt Service utils',  () => {

        var payload : {
            user: 1,
            role:1,
            nameRol:"admin"
          };


    test('valid method createToken', async() => {
        const value = await createToken(1,1,"admin");
        expect(value).toContain("");
    });


    test('valid method getToken', async() => {
        const value = await createToken(1,1,"admin");
        const getValue = await getToken(value);
        expect(getValue).toEqual({"nameRol": "admin", "role": 1, "sub": 1});

    });    

    test('valid method permission autenticate', async() => {
        const value = await createToken(1,1,"admin");
        const getValue = await getToken(value);
        const req = {
            headers: jest.fn().mockReturnValue({
                authorization:value,
            }),
          };
        const next = {};
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue({
                    message:"No tiene un Token Valido"
                }),
            }),
        }


    });        


});
