"use strict";
import {get_films,get_films_prom,set_film, accept_film, delete_film}  from "../../service/film.service"
import * as jwt from "../../utils/jwt";

/** 
* Listar Peliculas con paginacion y opcion de filtrar (Sin Autenticaciòn)
*/
export const  getFilms = async(req:any, res:any) => {
  const result = await get_films(req.body);
	return res.status(result.code).send(result);
}

/** 
 * Obtener una pelicula y su promedio de calificacion (Sin Autenticaciòn)
*/
export const  getFilmProm = async(req:any, res:any) => {
  const result = await get_films_prom(req.params);
	return res.status(result.code).send(result);
}

/** 
* Solicitar Agregar una Pelicula (Con autenticacion - user normal)
*/
export const  setFilm = async(req:any, res:any) => {
  let user = await jwt.getToken(req.headers.authorization.replace("Bearer ",""));
  const result = await set_film(req.body,user);
	return res.status(result.code).send(result);
}

/** 
* Aceptar pelicula a agregar (Con autenticacion -  admin)
*/
export const  acceptedFilm = async(req:any, res:any) => {
  let user = await jwt.getToken(req.headers.authorization.replace("Bearer ",""));
  const result = await accept_film(req.body,user);
	return res.status(result.code).send(result);
}

/** 
* Eliminar pelicula (Con autenticacion - admin)
*/
export const  deleteFilm = async(req:any, res:any) => {
  let user = await jwt.getToken(req.headers.authorization.replace("Bearer ",""));
  const result = await delete_film(req.body,user);
	return res.status(result.code).send(result);
}
