"use strict";
import {get_comments,set_comment,get_comment,delete_comment}  from "../../service/comment.service"
import * as jwt from "../../utils/jwt";

/** 
* Ver comentarios de peliculas paginado (Sin Autenticaciòn)
*/
export const  getComments = async(req:any, res:any) => {
  const result = await get_comments(req.params);
	return res.status(result.code).send(result);
}

/** 
 * Comentar una pelicula (Con autenticaciòn - usuario normal)
*/
export const  setComment = async(req:any, res:any) => {
  let user = await jwt.getToken(req.headers.authorization.replace("Bearer ",""));

  const result = await set_comment(req.body,user);
	return res.status(result.code).send(result);
}



/** 
 * Ver lista de comentarios por usuario y reviews (Usuario administrador)
*/
export const  getComment = async(req:any, res:any) => {
  const result = await get_comment(req.body);
	return res.status(result.code).send(result);
}


/** 
 * Eliminar comentario (Usuario Administrador)
*/
export const  deleteComment = async(req:any, res:any) => {
  const result = await delete_comment(req.body);
	return res.status(result.code).send(result);
}


