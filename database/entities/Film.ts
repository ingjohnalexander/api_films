
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm"

@Entity("films")
export class Film {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    description: string   
    
    @Column()
    image: string       

    @Column({nullable: true, default:1 })
    status: number

    @Column({nullable: true, default:2 })
    user_id: number

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;    
}