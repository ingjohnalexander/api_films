'use strict'
import { Router } from "express";
import * as authController from "./authController" 
import * as accountController from "./accountController" 

export const baseRoute = "/"+process.env.ROUTE+"auth";
export const authRoutes = (router: Router) => {
	router.get(baseRoute+"/",authController.index);	

	/**
	 * POST /auth/login
	 * @tags Auth
	 * @summary Create Autentication
	 * @description Autentication
	 * @param {object} request.body.required - info - application/json
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @example request - example user film
	 * {
	 *   "email": "jhon.chapid@gmail.com",
	 *   "password": "12345"
	 * }
	 * @example response - 200 - example user admin
	 * {
	 *   "username": "admin@gmail.com",
	 *   "password": "12345"
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.post(baseRoute+"/login",authController.login);

	/**
	 * POST /auth/register
	 * @tags Auth
	 * @summary Create Autentication
	 * @description Autentication
	 * @param {object} request.body.required - info - application/json
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @example request - example user film
	 * {
     *	"username":"usuario01",
     *	"password":"12345",
     *	"email":"usuario01@yahoo.es"
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */		
	router.post(baseRoute+"/register",accountController.accountRegister);
};
