"use strict";
import {register}  from "./../service/account.service"

export const accountRegister = async(req:any,res:any) => {
	const result = await register(req.body);
	return res.status(result.code).send(result);
}