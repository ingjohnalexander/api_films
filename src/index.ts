import express from 'express'
const app = express();
import * as dotenv from 'dotenv'
dotenv.config()
import cors from 'cors';
import * as bodyParser from "body-parser";
import {logger} from "./logger";
import { myDataSource } from "../database/database";
import expressJSDocSwagger from "express-jsdoc-swagger";
import { routes } from "./routes";

/**
 * Configuration Cors
 */
const allowedOrigins = ['http://localhost:4200'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.json());
app.use(cors(options));
const PORT = process.env.PORT;

app.get("/",(req,res) => {
    console.log("test ping");
    res.send("ok");
})


const handleErrors = (err:any, req:any, res:any, next:any) => {
    logger.log({level: 'info',message: err});
    res.status(200).send({
        success:'ERROR',
        message:'An internal server error occurred',
      }); 
}




// establish database connection
myDataSource
    .initialize()
    .then(() => {
        console.log("Data Source has been initialized!")
    })
    .catch((err) => {
        console.error("Error during Data Source initialization:", err)
    })

// import modules
routes(app);


/**
 * 
 */
const optionsSwagger = {
    info: {
        version: "1.0.0",
        title: "Test Backend",
        description: "Para ingresar a los endpoints con restriccion debe generar el token, y agregarlo en el authorization,<br>con ello podra acceder a dichos endpoints, ademas solo podra acceder segun el role si es un usuario administrador o un usuario normal",
        license: {
            name: ""
        }
    },
    security: {
        BearerAuth: {
          type: 'http',
          scheme: 'bearer',
        },
      },
    baseDir: __dirname,
    filesPattern: ["./../auth/**/*.ts","./../films/**/*.ts"],
    swaggerUIPath: "/api-docs",
    exposeSwaggerUI: true,
    exposeApiDocs: false,
    apiDocsPath: "/api-docs",
    notRequiredAsNullable: false,
    swaggerUiOptions: {},
    
    multiple: true
};

expressJSDocSwagger(app)(optionsSwagger);

app.use(handleErrors);
app.listen(PORT, () =>{
        console.log("listen");
})