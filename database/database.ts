import "reflect-metadata";
import { DataSource } from "typeorm"
import {User} from "./entities/User"
import {Film} from "./entities/Film"
import {Comment} from "./entities/Comment"
import {Role} from "./entities/Role"

/**
 * Config Data base
 */

export const myDataSource = new DataSource({
    type: "mysql",
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT+""),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB,
    entities: [User,Film,Comment,Role],
    logging: true,
    synchronize: true,    
})

