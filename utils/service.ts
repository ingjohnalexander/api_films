import * as bcrypt from "bcrypt";

var hashedPassword;

export const  setPass = async(password:string)  => {
    let hashedPassword = await bcrypt.hashSync(password, 10);
    return hashedPassword;
}

export const getPass = async(passUser:string, passDb:string) => {
    let hashedPassword = await bcrypt.compare(passUser, passDb);
    return hashedPassword;
}
