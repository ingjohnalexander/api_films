const request = require('supertest');
const express = require('express');

import {getComments,deleteComment,getComment,setComment} from "./../../films/controllers/commentController"
import { delete_comment,get_comment, set_comment,get_comments } from '../../service/comment.service';


const app = express();

jest.mock("../../database/database", () => {
	return {

		myDataSource: {
            initialize: jest.fn().mockReturnValue({
                then:jest.fn().mockReturnValue({
                    catch: jest.fn().mockReturnValue({}),
                }),
            }),
			getRepository: jest.fn().mockReturnValue({                
				find: jest.fn().mockReturnValue(["test"]),
                createQueryBuilder: jest.fn().mockReturnValue(["test"]),
				findOneBy: jest.fn().mockReturnValue({  }),
				save: jest.fn().mockReturnValue([]),
				count: jest.fn().mockReturnValue(1),
				delete: jest.fn().mockReturnValue([]),
			}),
			manager: {
              
				getRepository: jest.fn().mockReturnValue({

					find: jest.fn().mockReturnValue(["test"]),
					findOneBy: jest.fn().mockReturnValue({ posts: [] }),
					save: jest.fn().mockReturnValue([]),
					count: jest.fn().mockReturnValue(1),
					delete: jest.fn().mockReturnValue([]),
                    createQueryBuilder: jest.fn().mockReturnValue({
                        from: jest.fn().mockReturnValue({
                            select: jest.fn().mockReturnValue({
                                where: jest.fn().mockReturnValue({
                                    getCount:jest.fn().mockReturnValue([]),
                                }),
                            }),
                            where: jest.fn().mockReturnValue({
                                select: jest.fn().mockReturnValue({
                                    getRawMany:jest.fn().mockReturnValue([]),
                                    orderBy:jest.fn().mockReturnValue({
                                        getCount:jest.fn().mockReturnValue([]),
                                        getRawMany: jest.fn().mockReturnValue([]),
                                        limit: jest.fn().mockReturnValue({
                                            offset: jest.fn().mockReturnValue({
                                                getRawMany: jest.fn().mockReturnValue([])
                                            })
                                        }),
                                    })
                                })
                            }),
                        }),
                    })                    
				})
			},

            repository:{
				createQueryBuilder: jest.fn().mockReturnValue({
					select: jest.fn().mockReturnValue({
						where: jest.fn().mockReturnValue({
							distinct: jest.fn().mockReturnValue({
								getRawMany: jest.fn().mockReturnValue([])
							})
						})
					})
				})  
            }

              


		}
	};
});


let data = {
    message: 'Se ha eliminado el registro',
    code: 200,
    value: [],
    canNotify: false,
    error: false
  };

describe('valid Controller Comment',  () => {

    test('Ver comentarios de peliculas paginado (Sin Autenticaciòn)', async() => {
        const req = {  };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
        }
        getComments(req, res);
        data.message = "Lista Comentarioss";
        const value = await get_comments({page:1,id:1});
        expect(res.status(200).send(value)).toMatchObject(data);
    });

    test('Comentar una pelicula (Con autenticaciòn - usuario normal)', async() => {

        const req = { 
            headers:{
                authorization:""
            }            
         };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
            
        }
        const jwt = {
            getToken:jest.fn().mockReturnValue(""),
        }
        setComment(req, res);
        data.message = "Registro Exitoso";
        const value = await set_comment({descripcion:"description",qualification:2,film_id:1},{user:1});
        expect(res.status(200).send(value)).toMatchObject(data);
    });

    test('get comment - Ver lista de comentarios por usuario y reviews', async() => {
        const req = {  };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
        }
        getComment(req, res);
        data.message = "Lista de comentarios por usuario";
        const value = await get_comment({film_id:1,id:1});
        expect(res.status(200).send(value)).toMatchObject(data);
    });

    test('delete comment', async() => {
        const req = {  };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(data),
            }),
        }
        deleteComment(req, res);
        const value = await delete_comment({id:1});
        expect(res.status(200).send(value)).toMatchObject(data);
    });

});
