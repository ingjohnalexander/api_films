import "reflect-metadata";
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm"

@Entity("roles")
export class Role {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column({nullable: true, default:1 })
    status: number

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;    
}