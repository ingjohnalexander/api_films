'use strict'
import { Router } from "express";
import * as filmsController from "../controllers/filmController" 
import * as jwt from "../../utils/jwt";

export const baseRoute = "/"+process.env.ROUTE+"films";
export const routesFilms = (router: Router) => {

	/**
	 * POST /films
	 * @tags Films
	 * @summary get all films
	 * @description get all films
	 * @param {object} request.body.required - info - application/json
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @example request - example user film
	 * {
	 *   "page": "1",
	 *   "search": ""
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.post(baseRoute+"",filmsController.getFilms);

	/**
	 * GET /films/film-get/{id}
	 * @tags Films
	 * @summary get film for id
	 * @description get film for id
	 * @param {number} id.path - id
	 * @return {object} 200 - Success response
	 * }
	 * @example response - 200 - example success response
	 * {
	 *   "message": ""
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */

	router.get(baseRoute+"/film-get/:id",filmsController.getFilmProm);

	/**
	 * POST /films/film-set
	 * @tags Films
	 * @summary register film
	 * @description register film
	 * @param {object} request.body.required - info - application/json
	 * @security BasicAuth | BearerAuth
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @example request - example user film
	 * {
	    "name":"titulo pelicula",   
        "description":"descripcion pelicula",                
        "image":"pelicula"
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.post(baseRoute+"/film-set",jwt.authenticate([1,2]),filmsController.setFilm);

	/**
	 * POST /films/film-accepted
	 * @tags Films
	 * @summary accept film
	 * @description accept film
	 * @param {object} request.body.required - info - application/json
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @security BasicAuth | BearerAuth
	 * @example request - example user film
	 * {
	    "id":"1"
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.post(baseRoute+"/film-accepted",jwt.authenticate([1]),filmsController.acceptedFilm);

	/**
	 * DELETE /films/film-delete
	 * @tags Films
	 * @summary delete film for id
	 * @description delete film for id
	 * @param {object} request.body.required - info - application/json
	 * @return {object} 200 - Success response
	 * @return {object} 400 - Bad request response
	 * @security BasicAuth | BearerAuth
	 * @example request - example user film
	 * {
	    "id":"1"
	 * }
	 * @example response - 400 - example error response
	 * {
	 *   "message": "",
	 *   "errCode": "400"
	 * }
	 */	
	router.delete(baseRoute+"/film-delete",jwt.authenticate([1]),filmsController.deleteFilm);
	
};
