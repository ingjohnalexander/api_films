export interface CustomResponse {
	message: string;
	code: number;
	value: any;
	canNotify: boolean;
	error: boolean;
}

export interface FormError {
	field: string;
	message: string;
}

export interface ElementToFind {
	name: string;
	id: number;
}
