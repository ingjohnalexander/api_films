import "reflect-metadata";
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm"

@Entity("users")
export class User {
    @PrimaryGeneratedColumn()
    id: number

    @Column({nullable: false })
    username: string


    @Column({nullable: false })
    password: string   
    
    @Column({nullable: false })
    email: string       

    @Column({nullable: true, default:1 })
    status: number

    @Column({nullable: true, default:2 })
    roles_id: number

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;    
}