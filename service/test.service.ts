import { CustomResponse } from "../utils/interfaces";
import { Responses } from "../utils/responses";
import {myDataSource} from "../database/database";
import {Comment} from "../database/entities/Comment"
import {Film} from "../database/entities/Film"


/**
 * Listar Peliculas con paginacion y opcion de filtrar (Sin Autenticaciòn)
 */

export const get_films = async (body:any): Promise<CustomResponse> => {

    try {
          const repository = myDataSource.manager.getRepository(Film);
          let limit = parseInt(process.env.PAGINATION||"1");
          let skip = limit * body.page - limit;
          let search = "";
          if(body.search!=""){
              search = " AND f.name LIKE '%"+body.search+"%' ";
          }
  
          const elements = await repository.createQueryBuilder()
          .from("films", "f")
          .where("f.status = 1 "+search )
          .select(
            "f.name,f.description,f.image"
          )        
          .orderBy("f.id", "DESC")
          .limit(limit).offset(skip).getRawMany();
  
          const elementsCount = await repository.createQueryBuilder()
          .from("films", "f")
          .select(
            "COUNT(*) as total"
          )           
          .where("f.status = 1 "+search )     
          .getCount();
  
          return Responses.success({ value: {total:elementsCount,list:elements},message:"Lista Peliculas" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };

/**
 * Obtener una pelicula y su promedio de calificacion (Sin Autenticaciòn)
 */

export const get_films_prom = async (body:any): Promise<CustomResponse> => {

    try {
          const repository = myDataSource.manager.getRepository(Film);
          let id = body.id;
  
          const elements = await repository.createQueryBuilder()
          .from("films", "f")
          .where("f.id =  "+id)
          .select(
            "f.name,f.description,f.image, ((SELECT SUM(qualification) FROM comments WHERE film_id = f.id) / ( CASE WHEN (SELECT COUNT(qualification) FROM comments WHERE film_id = f.id) > 0 THEN (SELECT COUNT(qualification) FROM comments WHERE film_id = f.id) ELSE 1 END ) ) as prom"
          )   
          .getRawMany();
  
  
          return Responses.success({ value: elements,message:"Se ha obtenido la pelicula y su promedio" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };

/**
 * Solicitar Agregar una Pelicula (Con autenticacion - user normal)
 */

export const set_film = async (data:any,user:any): Promise<CustomResponse> => {

    try {
          
  
          const repository = myDataSource.manager.getRepository(Film);
      let message = "";
          var film = {
            name:data.name,   
            description:data.description,                
            image:data.image,  
            user_id:user.user,                      
            status: 0,
            create_at: new Date()
        };
  
  
          message =  data.name.length <=3 ? message+" - No ha agregado un nomnre a la pelicula valido": message+"" ;
          message =  data.description.length <=3 ? message+" - No ha agregado un nomnre a la pelicula valido": message+"" ;
  
          if(message!=""){
            return Responses.success({ value: [],message:message });
          }
  
          const elements = await repository.save(film);
  
          return Responses.success({ value: elements,message:"Registro Exitoso" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };
  

/**
 * Aceptar pelicula a agregar (Con autenticacion -  admin)
 */

export const accept_film = async (data:any,user:any): Promise<CustomResponse> => {

    try {
          const repository = myDataSource.manager.getRepository(Film);
          var film = await repository.findOneBy({id:data.id});
          film.status = 1;
          const elements = repository.save(film);
  
          return Responses.success({ value: elements,message:"Actualizaciòn Exitosa" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };

/**
 * Eliminar pelicula (Con autenticacion -  admin)
 */

export const delete_film = async (data:any,user:any): Promise<CustomResponse> => {

    try {
          const repository = myDataSource.manager.getRepository(Film);
          const repositoryComment = myDataSource.manager.getRepository(Comment);
  
          var comment = await repositoryComment.delete({film_id:data.id});
          var film = await repository.delete({id:data.id});
  
  
          return Responses.success({ value: film,message:"Se ha eliminado la pelicula y sus comentarios" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };
  
  