import "reflect-metadata";
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm"

@Entity("comments")
export class Comment {
    @PrimaryGeneratedColumn()
    id: number

    @Column({})
    description: string   

    @Column({})
    qualification: number  

    @Column({nullable: true, default:1 })
    status: number

    @Column({nullable: true, default:2 })
    user_id: number

    @Column({nullable: true, default:2 })
    film_id: number

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;    
}