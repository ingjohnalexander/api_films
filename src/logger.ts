import { createLogger, transports, format } from "winston";


export const logger = createLogger({
level: 'info',
format: format.json(),
transports: [
    new transports.File({ filename: 'logs/error.log', level: 'error' }),
    new transports.File({ filename: 'logs/combined.log' }),
],
});
  
logger.add(new transports.Console({
    format: format.simple(),
}));
  
