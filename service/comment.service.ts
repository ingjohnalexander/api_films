import { CustomResponse } from "../utils/interfaces";
import { Responses } from "../utils/responses";
import {myDataSource} from "../database/database";
import {Comment} from "../database/entities/Comment"



/**
 * Listar Comentarios  (Sin Autenticaciòn)
 */

export const get_comments = async (body:any): Promise<CustomResponse> => {

    try {
          const repository = myDataSource.manager.getRepository(Comment);

          let limit = parseInt(process.env.PAGINATION||"1");
          let skip = limit * body.page - limit;
         
            const elements = await myDataSource.createQueryBuilder().from("comments", "f")
            .where("f.status = 1 AND f.film_id = "+body.id )
            .select(
                "f.description,f.qualification,(SELECT username FROM users WHERE id = f.user_id LIMIT 1) as user"
            )
            .orderBy("f.id", "DESC").getRawMany(); 
            
            const elementsCount = await myDataSource.createQueryBuilder().from("comments", "f")
            .where("f.status = 1 AND f.film_id = "+body.id )
            .select(
                "f.description,f.qualification"
            )
            .orderBy("f.id", "DESC").getCount();             
  
          return Responses.success({ value: {total:elementsCount,list:elements},message:"Lista Comentarios" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };


/**
 *  Comentar una pelicula (Con autenticaciòn - usuario normal)
 */

export const set_comment = async (data:any,user:any): Promise<CustomResponse> => {

    try {
          
          const repository = myDataSource.manager.getRepository(Comment);
          let message = "";
          var comment = {
            description:data.description,      
            status:0,
            qualification:data.qualification,
            user_id:user.user,          
            film_id:data.film_id,
            create_at: new Date()  
        };
  
  
  
          message =  data.description.length <=3 ? message+" - No ha agregado una descripcion": message+"" ;
          message =  data.qualification == "" ? message+" - Debe ingresar un valor de calificacion valido": message+"" ;
  
          if(message!=""){
            return Responses.success({ value: [],message:message });
          }
  
          const elements = await repository.save(comment);
  
          return Responses.success({ value: elements,message:"Registro Exitoso" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };


/**
 *  Ver lista de comentarios por usuario y reviews (Usuario administrador)
 */

export const get_comment = async (data:any): Promise<CustomResponse> => {

    try {
          
          const repository = myDataSource.manager.getRepository(Comment);
         
          const elements = await myDataSource.createQueryBuilder().from("comments", "f")
              .where("f.film_id =  "+data.film_id+" AND f.user_id = "+data.id)
              .select(
                  "f.description,f.qualification,(SELECT username FROM users WHERE id = f.user_id LIMIT 1) as user"
              )
              .orderBy("f.id", "DESC").getRawMany();
  
          return Responses.success({ value: elements,message:"Lista de comentarios por usuario" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  
  };
  

  /**
 *  Eliminar comentario (Usuario Administrador)
 */

export const delete_comment = async (data:any): Promise<CustomResponse> => {
    try {
        const repository = myDataSource.manager.getRepository(Comment);   
        var comment = await repository.delete({id:data.id});
        return Responses.success({ value: [],message:"Se ha eliminado el registro" });
      } catch (error:any) {
          return Responses.internalServerError({ message: error?.message });
      }
  };
  