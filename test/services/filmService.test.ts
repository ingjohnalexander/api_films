

import request from 'supertest';
import express from 'express';
import { delete_film, accept_film, set_film, get_films_prom, get_films } from '../../service/film.service';
import { describe, expect } from "@jest/globals";

const app =  express();


jest.mock("../../database/database", () => {
	return {

		myDataSource: {
            createQueryBuilder: jest.fn().mockReturnValue({
                from: jest.fn().mockReturnValue({
                    select: jest.fn().mockReturnValue({
                        where: jest.fn().mockReturnValue({
                            getCount:jest.fn().mockReturnValue([]),
                        }),
                    }),
                    where: jest.fn().mockReturnValue({
                        select: jest.fn().mockReturnValue({
                            getRawMany:jest.fn().mockReturnValue([]),
                            orderBy:jest.fn().mockReturnValue({
                                getCount:jest.fn().mockReturnValue([]),
                                getRawMany: jest.fn().mockReturnValue([]),
                                limit: jest.fn().mockReturnValue({
                                    offset: jest.fn().mockReturnValue({
                                        getRawMany: jest.fn().mockReturnValue([])
                                    })
                                }),
                            })
                        })
                    }),
                }),
            }),
			getRepository: jest.fn().mockReturnValue({                
				find: jest.fn().mockReturnValue(["test"]),
				findOneBy: jest.fn().mockReturnValue({  }),
				save: jest.fn().mockReturnValue([]),
				count: jest.fn().mockReturnValue(1),
				delete: jest.fn().mockReturnValue([]),
                createQueryBuilder: jest.fn().mockReturnValue({
                    from: jest.fn().mockReturnValue({
                        select: jest.fn().mockReturnValue({
                            where: jest.fn().mockReturnValue({
                                getCount:jest.fn().mockReturnValue([]),
                            }),
                        }),
                        where: jest.fn().mockReturnValue({
                            select: jest.fn().mockReturnValue({
                                getRawMany:jest.fn().mockReturnValue([]),
                                orderBy:jest.fn().mockReturnValue({
                                    getCount:jest.fn().mockReturnValue([]),
                                    getRawMany: jest.fn().mockReturnValue([]),
                                    limit: jest.fn().mockReturnValue({
                                        offset: jest.fn().mockReturnValue({
                                            getRawMany: jest.fn().mockReturnValue([])
                                        })
                                    }),
                                })
                            })
                        }),
                    }),
                })  
			}),
			manager: {
              
				getRepository: jest.fn().mockReturnValue({

					find: jest.fn().mockReturnValue(["test"]),
					findOneBy: jest.fn().mockReturnValue({ posts: [] }),
					save: jest.fn().mockReturnValue([]),
					count: jest.fn().mockReturnValue(1),
					delete: jest.fn().mockReturnValue([]),
                    createQueryBuilder: jest.fn().mockReturnValue({
                        from: jest.fn().mockReturnValue({
                            select: jest.fn().mockReturnValue({
                                where: jest.fn().mockReturnValue({
                                    getCount:jest.fn().mockReturnValue([]),
                                }),
                            }),
                            where: jest.fn().mockReturnValue({
                                select: jest.fn().mockReturnValue({
                                    getRawMany:jest.fn().mockReturnValue([]),
                                    orderBy:jest.fn().mockReturnValue({
                                        getCount:jest.fn().mockReturnValue([]),
                                        getRawMany: jest.fn().mockReturnValue([]),
                                        limit: jest.fn().mockReturnValue({
                                            offset: jest.fn().mockReturnValue({
                                                getRawMany: jest.fn().mockReturnValue([])
                                            })
                                        }),
                                    })
                                })
                            }),
                        }),
                    })                    
				})
			},

            repository:{
				createQueryBuilder: jest.fn().mockReturnValue({
					select: jest.fn().mockReturnValue({
						where: jest.fn().mockReturnValue({
							distinct: jest.fn().mockReturnValue({
								getRawMany: jest.fn().mockReturnValue([])
							})
						})
					})
				})  
            }

              


		}
	};
});





describe('Service Film', function () {

    test('Listar Peliculas con paginacion y opcion de filtrar (Sin Autenticaciòn)', async () => {
        const value = await get_films({page:1,search:"asdasd"});
        expect(value.message).toBe("Lista Peliculas");        
    }); 

    test('Obtener una pelicula y su promedio de calificacion (Sin Autenticaciòn)', async () => {
        const value = await get_films_prom({id:1});
        expect(value.message).toBe("Se ha obtenido la pelicula y su promedio");        
    }); 

    test('Solicitar Agregar una Pelicula (Con autenticacion - user normal)', async () => {
        const value = await set_film({description:"descscsc",image:"---",name:"name Pelicula"},{user:1});
        expect(value.message).toBe("Registro Exitoso");        
    });   

    test('Aceptar pelicula a agregar (Con autenticacion -  admin)', async () => {
        const value = await accept_film({id:1},{user:1});
        expect(value.message).toBe("Actualizaciòn Exitosa");        
    });    

    test('Eliminar pelicula (Con autenticacion -  admin)', async () => {
        const value = await delete_film({id:1},{user:1});
        expect(value.message).toBe("Se ha eliminado la pelicula y sus comentarios");        
    });
    
  });