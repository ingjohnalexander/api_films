const request = require('supertest');
const express = require('express');

import {login} from "../../auth/authController"
import {auth}  from "../../service/auth.service"


const app = express();

jest.mock("../../database/database", () => {
	return {

		myDataSource: {
            initialize: jest.fn().mockReturnValue({
                then:jest.fn().mockReturnValue({
                    catch: jest.fn().mockReturnValue({}),
                }),
            }),
			getRepository: jest.fn().mockReturnValue({                
				find: jest.fn().mockReturnValue(["test"]),
                createQueryBuilder: jest.fn().mockReturnValue(["test"]),
				findOneBy: jest.fn().mockReturnValue({  }),
				save: jest.fn().mockReturnValue([]),
				count: jest.fn().mockReturnValue(1),
				delete: jest.fn().mockReturnValue([]),
			}),
			manager: {
              
				getRepository: jest.fn().mockReturnValue({

					find: jest.fn().mockReturnValue(["test"]),
					findOneBy: jest.fn().mockReturnValue({ posts: [] }),
					save: jest.fn().mockReturnValue([]),
					count: jest.fn().mockReturnValue(1),
					delete: jest.fn().mockReturnValue([]),
                    createQueryBuilder: jest.fn().mockReturnValue({
                        from: jest.fn().mockReturnValue({
                            select: jest.fn().mockReturnValue({
                                where: jest.fn().mockReturnValue({
                                    getCount:jest.fn().mockReturnValue([]),
                                }),
                            }),
                            where: jest.fn().mockReturnValue({
                                select: jest.fn().mockReturnValue({
                                    getRawMany:jest.fn().mockReturnValue([]),
                                    orderBy:jest.fn().mockReturnValue({
                                        getCount:jest.fn().mockReturnValue([]),
                                        getRawMany: jest.fn().mockReturnValue([]),
                                        limit: jest.fn().mockReturnValue({
                                            offset: jest.fn().mockReturnValue({
                                                getRawMany: jest.fn().mockReturnValue([])
                                            })
                                        }),
                                    })
                                })
                            }),
                        }),
                    })                    
				})
			},

            repository:{
				createQueryBuilder: jest.fn().mockReturnValue({
					select: jest.fn().mockReturnValue({
						where: jest.fn().mockReturnValue({
							distinct: jest.fn().mockReturnValue({
								getRawMany: jest.fn().mockReturnValue([])
							})
						})
					})
				})  
            }

              


		}
	};
});


let _response = {
    message: 'Se ha eliminado el registro',
    code: 200,
    value: [],
    canNotify: false,
    error: false
  };

describe('valid Controller Auth',  () => {

    test('Login', async() => {
        const req = { 
            body:{
                email:"jhon.chapid@gmail.com",password:"123456"
            }
         };
        const res =  {
            status: jest.fn().mockReturnValue({
                send:jest.fn().mockReturnValue(_response),
            }),
        }
        login(req, res);
        _response.message = "Logueado";
        const value = await auth("jhon.chapid@gmail.com","12345");
        expect(res.status(200).send(value)).toMatchObject(_response);
    });

});
