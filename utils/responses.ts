import { CustomResponse } from "./interfaces";
import express from 'express'
const app = express();
/**
 * Allows empty fields
 *
 * All response values are in false by default
 *
 * @Returns CustomResponse
 */
export abstract class Responses {

	/**
	 *HTTP 200
	 */
     static success({
		message = "",
		value = {},
		canNotify = false,
		error = false,
		res=null
	}): CustomResponse {
		let result = {
			message: message,
			code: 200,
			value: value,
			canNotify: canNotify,
			error: error
		};
		if (!res) return result;
        res.status(result.code).send(result);
	}

	/**
	 *HTTP 201
	 */
    
	static created({
		message = "",
		value=null,
		canNotify = false,
		res = null
	}): CustomResponse {
		const result = {
			message: message,
			code: 201,
			value: value,
			canNotify: canNotify,
			error: false,
		};
		if (!res) return result;
		return res.status(result.code).send(result);
	}

	/**
	 *HTTP 400
	 */
    
	static badRequest({
		message="",
		value = {},
		canNotify = false,
		error = true,
		res = null
	}): CustomResponse {
		const result = {
			message: message,
			code: 400,
			value: value,
			canNotify: canNotify,
			error: error,
		};
		if (!res) return result;
		return res.status(result.code).send(result);
	}

	/**
	 *HTTP 404
	 */
    
	static notFound({
		message="",
		value = {},
		canNotify = false,
		error = false,
		res = null
	}): CustomResponse {
		const result = {
			message: message,
			code: 404,
			value: value,
			canNotify: canNotify,
			error: error,
		};
		if (!res) return result;
		return res.status(result.code).send(result);
	}

	/**
	 *HTTP 500
	 */
    
	static internalServerError({
		message = "",
		value = {},
		canNotify = false,
		res=null,
	}): CustomResponse {
		const result = {
			message: message,
			code: 500,
			value: value,
			canNotify: canNotify,
			error: true,
		};
		if (!res) return result;
		return res.status(result.code).send(result);
	}
    
}
