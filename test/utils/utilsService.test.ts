import {getPass,setPass} from "../../utils/service"
describe('setPass Service utils',  () => {

    test('valid method getPass', async() => {
        const get_pass = await getPass("12345","$2b$10$f7ldz6RkmcaMYUTjIAADOOdCFFkg6fmXxXXDvLT6NxiUu9uIj.n0C");
        expect(get_pass).toEqual(true);
    });

    test('valid method setPass', async() => {
        const set_pass = await setPass("12345");
        const get_pass = await getPass("12345",set_pass);
        expect(get_pass).toEqual(true);
    });

});
