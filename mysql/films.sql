/*
SQLyog Trial v13.1.9 (64 bit)
MySQL - 8.0.30 : Database - films
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`films` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `films`;

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` int DEFAULT '1',
  `qualification` int NOT NULL,
  `film_id` int DEFAULT '2',
  `user_id` int DEFAULT '2',
  `description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `comments` */

insert  into `comments`(`id`,`status`,`qualification`,`film_id`,`user_id`,`description`,`created_at`,`updated_at`) values 
(1,1,3,12,2,'comentario 01','2023-05-01 12:37:27.395457','2023-05-01 23:54:19.451873'),
(11,1,3,11,2,'comentario 011','2023-05-01 23:45:32.987042','2023-05-01 23:54:23.395477');

/*Table structure for table `films` */

DROP TABLE IF EXISTS `films`;

CREATE TABLE `films` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `status` int DEFAULT '1',
  `user_id` int DEFAULT '2',
  `description` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `films` */

insert  into `films`(`id`,`name`,`image`,`status`,`user_id`,`description`,`created_at`,`updated_at`) values 
(4,'titulo pelicula 04','pelicula',1,2,'descripcion pelicula','2023-04-30 21:38:45.017116','2023-05-01 23:09:03.148963'),
(6,'titulo pelicula 06','pelicula',1,2,'descripcion pelicula','2023-04-30 21:39:30.525877','2023-05-01 23:04:01.124288'),
(7,'titulo pelicula 07','pelicula',0,2,'descripcion pelicula','2023-04-30 22:28:45.734574','2023-05-01 23:04:04.806885'),
(8,'titulo pelicula 08','pelicula',0,2,'descripcion pelicula','2023-04-30 22:28:49.183658','2023-05-01 23:04:08.148942'),
(9,'titulo pelicula 09','pelicula',0,2,'descripcion pelicula','2023-04-30 22:28:51.738526','2023-05-01 23:04:12.070192'),
(10,'titulo pelicula 010','pelicula',0,2,'descripcion pelicula','2023-04-30 22:29:16.838311','2023-05-01 23:04:17.350665'),
(11,'titulo pelicula 011','pelicula',0,2,'descripcion pelicula','2023-04-30 22:31:41.302453','2023-05-01 23:04:21.366037'),
(12,'titulo pelicula 012','pelicula',1,2,'descripcion pelicula','2023-04-30 22:45:36.125684','2023-05-01 23:04:24.676336');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `status` int DEFAULT '1',
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`status`,`created_at`,`updated_at`) values 
(1,'ADMIN',1,'2023-04-30 21:38:45.079112','2023-05-01 23:00:29.920950'),
(2,'USER',1,'2023-05-01 22:58:18.000000','2023-05-01 23:00:25.900226');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `status` int DEFAULT '1',
  `roles_id` int DEFAULT '2',
  `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`password`,`email`,`status`,`roles_id`,`username`,`created_at`,`updated_at`) values 
(1,'$2b$10$f7ldz6RkmcaMYUTjIAADOOdCFFkg6fmXxXXDvLT6NxiUu9uIj.n0C','jhon.chapid@gmail.com',1,2,'jhon','2023-04-30 19:03:29.108044','2023-05-01 23:00:46.903000'),
(2,'$2b$10$f7ldz6RkmcaMYUTjIAADOOdCFFkg6fmXxXXDvLT6NxiUu9uIj.n0C','admin@gmail.com',1,1,'jhon','2023-04-30 19:03:29.108044','2023-05-01 23:00:58.246707'),
(22,'$2b$10$QDDmoX9gfwwAebZsR5mL9uZE8TSdRCl4F9Msnx3uSpt1CN1/2ml92','usuario01@gmail.com',1,2,'usuario01','2023-05-01 22:57:29.669400','2023-05-01 22:57:29.669400'),
(23,'$2b$10$tiX830pkL2AE/55tUStOHOdd6sf4.rcvaXfA.L9yFi/XD7YnvIN4i','usuario02@gmail.com',1,2,'usuario02','2023-05-01 22:57:40.753556','2023-05-01 22:57:40.753556');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
