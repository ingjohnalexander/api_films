

import request from 'supertest';
import express from 'express';
import { delete_comment,get_comment, set_comment,get_comments } from '../../service/comment.service';
import { describe, expect } from "@jest/globals";

const app =  express();


jest.mock("../../database/database", () => {
	return {

		myDataSource: {
            createQueryBuilder: jest.fn().mockReturnValue({
                from: jest.fn().mockReturnValue({
                    select: jest.fn().mockReturnValue({
                        where: jest.fn().mockReturnValue({
                            getCount:jest.fn().mockReturnValue([]),
                        }),
                    }),
                    where: jest.fn().mockReturnValue({
                        select: jest.fn().mockReturnValue({
                            getRawMany:jest.fn().mockReturnValue([]),
                            orderBy:jest.fn().mockReturnValue({
                                getCount:jest.fn().mockReturnValue([]),
                                getRawMany: jest.fn().mockReturnValue([]),
                                limit: jest.fn().mockReturnValue({
                                    offset: jest.fn().mockReturnValue({
                                        getRawMany: jest.fn().mockReturnValue([])
                                    })
                                }),
                            })
                        })
                    }),
                }),
            }),
			getRepository: jest.fn().mockReturnValue({                
				find: jest.fn().mockReturnValue(["test"]),
				findOneBy: jest.fn().mockReturnValue({  }),
				save: jest.fn().mockReturnValue([]),
				count: jest.fn().mockReturnValue(1),
				delete: jest.fn().mockReturnValue([]),
                createQueryBuilder: jest.fn().mockReturnValue({
                    from: jest.fn().mockReturnValue({
                        select: jest.fn().mockReturnValue({
                            where: jest.fn().mockReturnValue({
                                getCount:jest.fn().mockReturnValue([]),
                            }),
                        }),
                        where: jest.fn().mockReturnValue({
                            select: jest.fn().mockReturnValue({
                                getRawMany:jest.fn().mockReturnValue([]),
                                orderBy:jest.fn().mockReturnValue({
                                    getCount:jest.fn().mockReturnValue([]),
                                    getRawMany: jest.fn().mockReturnValue([]),
                                    limit: jest.fn().mockReturnValue({
                                        offset: jest.fn().mockReturnValue({
                                            getRawMany: jest.fn().mockReturnValue([])
                                        })
                                    }),
                                })
                            })
                        }),
                    }),
                })  
			}),
			manager: {
              
				getRepository: jest.fn().mockReturnValue({

					find: jest.fn().mockReturnValue(["test"]),
					findOneBy: jest.fn().mockReturnValue({ posts: [] }),
					save: jest.fn().mockReturnValue([]),
					count: jest.fn().mockReturnValue(1),
					delete: jest.fn().mockReturnValue([]),
                    createQueryBuilder: jest.fn().mockReturnValue({
                        from: jest.fn().mockReturnValue({
                            select: jest.fn().mockReturnValue({
                                where: jest.fn().mockReturnValue({
                                    getCount:jest.fn().mockReturnValue([]),
                                }),
                            }),
                            where: jest.fn().mockReturnValue({
                                select: jest.fn().mockReturnValue({
                                    getRawMany:jest.fn().mockReturnValue([]),
                                    orderBy:jest.fn().mockReturnValue({
                                        getCount:jest.fn().mockReturnValue([]),
                                        getRawMany: jest.fn().mockReturnValue([]),
                                        limit: jest.fn().mockReturnValue({
                                            offset: jest.fn().mockReturnValue({
                                                getRawMany: jest.fn().mockReturnValue([])
                                            })
                                        }),
                                    })
                                })
                            }),
                        }),
                    })                    
				})
			},

            repository:{
				createQueryBuilder: jest.fn().mockReturnValue({
					select: jest.fn().mockReturnValue({
						where: jest.fn().mockReturnValue({
							distinct: jest.fn().mockReturnValue({
								getRawMany: jest.fn().mockReturnValue([])
							})
						})
					})
				})  
            }

              


		}
	};
});


import { myDataSource } from "./../../database/database"
describe('Service Comment', function () {


    test('get_comments - Listar Peliculas con paginacion y opcion de filtrar', async () => {
        const value = await get_comments({page:1,id:1});
        expect(value.message).toBe("Lista Comentarios");        
    });

    test('set_comment - Comentar una pelicula', async () => {
        const value = await set_comment({description:"description",qualification:2,film_id:1},{user:1});
        expect(value.message).toBe("Registro Exitoso");        
    });

    test('get comment - Ver lista de comentarios por usuario y reviews', async () => {
        const value = await get_comment({film_id:1,id:1});
        expect(value.message).toBe("Lista de comentarios por usuario");        
    });

    test('delete comment', async () => {
        const value = await delete_comment({id:1});
        expect(value.message).toBe("Se ha eliminado el registro");        
    });
    
  });